// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBSbNpnwsqSbznMqpwBQ-IGeI21T-cTD-E',
        authDomain: 'postr-cd21d.firebaseapp.com',
        databaseURL: 'https://postr-cd21d.firebaseio.com',
        projectId: 'postr-cd21d',
        storageBucket: 'postr-cd21d.appspot.com',
        messagingSenderId: '120133362805',
        appId: '1:120133362805:web:59bac526d7f8fd7824043c',
        measurementId: 'G-YC142FN0JM'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
