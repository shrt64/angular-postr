import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faBars, faUser } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '@service/auth/auth.service';
import { MenuService } from '@service/menu.service';
import { User } from 'firebase/app';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public faBars = faBars;
    public faUser = faUser;
    public menuActive: boolean;
    public user: User;

    constructor(
        public menuService: MenuService,
        private authService: AuthService,
        public router: Router
    ) { }

    ngOnInit() {
        this.menuService.menuIsVisible.subscribe(res => this.menuActive = res);
        this.authService.userState().subscribe(user => this.user = user);
    }

    toggleLogin() {
        if (this.user) {
            console.log(this.user.uid);

            this.router.navigate(['/Profile']);
        } else {
            this.router.navigate(['/Login']);
            this.menuService.menuIsVisible.next(false);
        }
    }
}
