import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@service/auth/auth.service';
import { User } from 'firebase';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {
    public userForm: FormGroup;
    public loginErrorCode: string;
    public backgroundClassList: string;
    public fadeInOpacity: string;
    private user: User;
    constructor(
        public router: Router,
        private authService: AuthService,
    ) { }

    ngOnInit() {
        this.backgroundClassList = 'ui-grid center';
        this.authService.userState().subscribe(user => this.user = user);
        this.userForm = new FormGroup({
            username: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required])
        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.fadeInOpacity = '1';
        }, 200);
    }

    ngOnDestroy() {
        this.fadeInOpacity = '0';
    }

    onCancel() {

    }

    onSubmit(pFormValue: Form) {
        const loadingOpacityElement = document.getElementById('formLoadingOpacity');
        this.userForm.disable();
        this.backgroundClassList = 'ui-grid center formLoadingBackground';
        loadingOpacityElement.style.opacity = '0.5';
        setTimeout(() => {
            this.authService.loginWithEmail(pFormValue).then(error => {
                if (error.code) {
                    console.log(error.code);
                    const valueTemp = this.userForm.value;
                    this.userForm.reset();
                    this.userForm.setValue(valueTemp);
                    this.loginErrorCode = error.code;
                    this.userForm.enable();
                } else {
                    this.router.navigate(['/Posts']);
                }
                this.backgroundClassList = 'ui-grid center';
                loadingOpacityElement.style.opacity = '1';
            });
        }, 1000);
        // if (this.user) this.router.navigate(['/Posts']);
    }

    canDeactivate(): Observable<boolean> | boolean {
        if (this.userForm.valid) {
            return true;
        } else {
            return false;
        }
    }
}
