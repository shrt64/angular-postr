import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Form, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@service/auth/auth.service';
import { User } from 'firebase';

export const passwordMismatchCheck: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const passwordConfirm = control.get('passwordConfirm');
    return password.value !== passwordConfirm.value ? { passwordMismacth: true } : null;
};

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit, AfterViewInit {
    public userForm: FormGroup;
    public loginErrorCode: string;
    public backgroundClassList: string;
    public fadeInOpacity: string;
    public user: User;
    constructor(
        public router: Router,
        private authService: AuthService,
    ) { }

    ngOnInit() {
        this.backgroundClassList = 'ui-grid center';
        this.authService.userState().subscribe(user => this.user = user);
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.fadeInOpacity = '1';
        }, 200);
    }

    onSubmit(pFormValue: Form) {
        const loadingOpacityElement = document.getElementById('formLoadingOpacity');
        this.userForm.disable();
        this.backgroundClassList = 'ui-grid center formLoadingBackground';
        loadingOpacityElement.style.opacity = '0.5';
        setTimeout(() => { }, 1000);
        // if (this.user) this.router.navigate(['/Posts']);
    }

    logOut() {
        console.log(this.user);
        this.authService.logout();
        console.log(this.authService.userState());

        this.router.navigate(['/Posts']);
    }

}
