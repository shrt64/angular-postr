import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@service/auth/auth.service';
import { User } from 'firebase';

export const passwordMismatchCheck: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const passwordConfirm = control.get('passwordConfirm');
    return password.value !== passwordConfirm.value ? { passwordMismacth: true } : null;
};
@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit, AfterViewInit {
    public userForm: FormGroup;
    public loginErrorCode: string;
    public backgroundClassList: string;
    public fadeInOpacity: string;
    private user: User;
    constructor(
        public router: Router,
        private authService: AuthService,
    ) { }

    ngOnInit() {
        this.backgroundClassList = 'ui-grid center';
        this.authService.userState().subscribe(user => this.user = user);
        this.userForm = new FormGroup({
            username: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            passwordConfirm: new FormControl('', [Validators.required])
        }, { validators: passwordMismatchCheck });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.fadeInOpacity = '1';
        }, 200);
    }

    onSubmit(pFormValue: Form) {
        const loadingOpacityElement = document.getElementById('formLoadingOpacity');
        this.userForm.disable();
        this.backgroundClassList = 'ui-grid center formLoadingBackground';
        loadingOpacityElement.style.opacity = '0.5';
        setTimeout(() => {
            this.authService.registerWithEmail(pFormValue).then(error => {
                if (error.code) {
                    console.log(error.code);
                    const valueTemp = this.userForm.value;
                    this.userForm.reset();
                    this.userForm.setValue(valueTemp);
                    this.loginErrorCode = error.code;
                    this.userForm.enable();
                } else {
                    this.router.navigate(['/Posts']);
                }
                this.backgroundClassList = 'ui-grid center';
                loadingOpacityElement.style.opacity = '1';
            });
        }, 1000);
        // if (this.user) this.router.navigate(['/Posts']);
    }

}
