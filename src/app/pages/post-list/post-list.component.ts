import { AfterViewChecked, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@service/auth/auth.service';
import { DbService } from '@service/db/db.service';
import { MessageService } from 'primeng/api';

export interface Item { name: string; }

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.scss'],
    providers: [MessageService]
})

export class PostListComponent implements OnInit, AfterViewChecked {
    public loginErrorCode: string;
    public backgroundClassList: string;
    public fadeInOpacity: string;
    public posts: any;
    public userState: any;
    disableAnimations = true;
    trackPosts = (idx, obj) => obj.id;

    constructor(
        private dbService: DbService,
        private authService: AuthService,
        public router: Router,
        private messageService: MessageService,
        public changeDetectorRef: ChangeDetectorRef
    ) {
        this.dbService.posts.subscribe(posts => this.posts = posts);
        this.authService.userState().subscribe(userState => this.userState = userState);
    }

    ngOnInit() { }

    ngAfterViewChecked(): void { }

    addItem($event: any) {
        if (this.userState) {
            const post = {
                userId: this.userState.uid,
                submitDate: new Date(),
                text: $event.target.value,
                userEmail: 'email'
            };
            this.dbService.addItem(post);
        }
    }

    delItem(element, pPostId) {
        console.log(element);
        element.target.parentElement.parentElement.style.opacity = '0';
        element.target.parentElement.parentElement.style.transition = 'visibility 0s 2s, opacity 2s linear;';
        setTimeout(() => {
            this.dbService.delItem(pPostId);
        }, 200);
    }
    delAnimEnd(pPostId) {
        this.dbService.delItem(pPostId);
    }
    bum() {

    }
}
