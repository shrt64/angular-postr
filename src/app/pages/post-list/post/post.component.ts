import { animate, keyframes, style, transition, trigger } from '@angular/animations';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@service/auth/auth.service';
import { DbService } from '@service/db/db.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss'],
    animations: [
        trigger('fadeInOut', [
            transition('void => added', [
                style({ transform: 'translateX(-100%)', opacity: 0 }),
                animate(300, style({ transform: 'translateX(0)', opacity: 1 }))
            ]),
            transition('added => modified', animate('200ms', keyframes([
                style({ transform: 'scale(1.1)', backgroundColor: '#e5eaf0', boxShadow: '0 0 5px #e5eaf0', offset: 0.2 }),
                style({ transform: 'scale(1)', backgroundColor: 'initial', boxShadow: '1px 2px 10px -1px #000', offset: 1 })
            ]))),
            transition('* => deleted', [
                style({ transform: 'translateX( 100%)' }),
                animate(10000)
            ])
        ])
    ]
})
export class PostComponent implements OnInit {
    @Input() post: any;
    public userState: any;
    public userEmail: string;

    constructor(
        private dbService: DbService,
        private authService: AuthService,
        public router: Router,
        public changeDetectorRef: ChangeDetectorRef) {
        this.authService.userState().subscribe(userState => this.userState = userState);
    }

    ngOnInit() {
        console.log(this.post);
        this.post.userEmailObs.subscribe(email => this.userEmail = email);
    }

    updateItem(pPostId: string, pSubmitDate: Date) {
        const post = {
            userId: this.userState.uid,
            submitDate: pSubmitDate,
            text: 'Frissítetted a posztot!'
        };
        console.log(post.userId);

        this.dbService.updateItem(post, pPostId);
    }

    delItem(element, pPostId) {
        console.log(element);
        element.target.parentElement.parentElement.style.opacity = '0';
        element.target.parentElement.parentElement.style.transition = 'visibility 0s 2s, opacity 2s linear;';
        setTimeout(() => {
            this.dbService.delItem(pPostId);
        }, 200);
    }

    getPostUser(pUid: string) {
        this.dbService.getUser(pUid);
    }
}
