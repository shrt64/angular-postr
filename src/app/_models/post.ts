export class Post {
    userId: string;
    submitDate: Date;
    text: string;
}
