import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './pages/auth/login/login.component';
import { ProfileComponent } from './pages/auth/profile/profile.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { PostListComponent } from './pages/post-list/post-list.component';


const routes: Routes = [
    { path: 'Login', component: LoginComponent },
    { path: 'Posts', component: PostListComponent },
    { path: 'Register', component: RegisterComponent },
    { path: 'Profile', component: ProfileComponent }
    // { path: '**', redirectTo: 'users' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
