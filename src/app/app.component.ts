import { Component, OnInit } from '@angular/core';
import { MenuService } from '@service/menu.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public items: any;
    title = '-= PostR =-';
    public menuActive: boolean;

    constructor(
        private menuService: MenuService) {
    }

    ngOnInit() {
        this.menuService.menuIsVisible.subscribe(res => this.menuActive = res);
    }
}
