import 'firebase/firestore';

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Post } from '@model/post';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface PostId extends Post { id: string; }

@Injectable({
    providedIn: 'root'
})

export class DbService {
    private postCollection: AngularFirestoreCollection<Post>;
    private userCollection: AngularFirestoreCollection<any>;
    public posts: Observable<any>;
    public users: Observable<any>;
    public postLogs: Observable<PostId[]>;
    public type: string;

    constructor(
        public auth: AngularFireAuth,
        public firestore: AngularFirestore
    ) {
        this.postCollection = firestore.collection<Post>('posts', ref => ref.orderBy('submitDate'));
        // cachelni és külön obs rá
        const getUserIdFromUsers = (pUserId: string) => {
            const userDoc$ = firestore.doc<any>(`users/${pUserId}`).get();
            return userDoc$.pipe(map(x => x.data().email));
        };

        this.posts = this.postCollection.snapshotChanges().pipe(
            map(actions => actions.map(a => {
                const data = a.payload.doc.data();
                const id = a.payload.doc.id;
                const dbState = a.type;
                const userEmailObs = getUserIdFromUsers(a.payload.doc.data().userId);
                return { id, dbState, data, userEmailObs };
            }))
        );

        this.postLogs = this.postCollection.auditTrail().pipe(
            map(actions => actions.map(a => {
                const data = a.payload.doc.data() as Post;
                const id = a.payload.doc.id;
                return { id, ...data };
            }))
        );
    }

    getPosts(): Observable<any> {
        return this.posts;
    }

    getPostsSnapshot(): Observable<any> {
        return this.postCollection.snapshotChanges();
    }

    addItem(pPost: Post) {
        this.postCollection.add(pPost).then(alma => console.log('Post created: ' + alma.id));
    }

    updateItem(pPost: Post, pPostId: string) {
        this.postCollection.doc(pPostId).update(pPost).then(alma => console.log(pPost));
    }

    delItem(pPostId: string) {
        this.postCollection.doc(pPostId).delete().then(alma => console.log('Post deleted: ' + pPostId));
    }

    getUser(pUid: string) {
        // console.log(this.postCollection.doc('7SLDIXQhaOhwrraEyaE1Z0QNLGk1').get());
    }

}
