// import { HttpClient } from '@angular/common/http';
// import { Injectable } from '@angular/core';
// import { User } from '@model/user';
// import { Observable, of } from 'rxjs';

// @Injectable({
//     providedIn: 'root'
// })
// export class UserService {
//     public userIdBuffer: number;
//     private userList: User[] = localStorage.getItem('userList') ? JSON.parse(localStorage.getItem('userList')) : [
//         { id: 0, name: 'Pisti', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 1, name: 'Pisti', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 2, name: 'Feri', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 3, name: 'Béla', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 4, name: 'Kristóf', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 5, name: 'Sanyi', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 6, name: 'Peti', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 7, name: 'Otto', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 8, name: 'Judit', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 9, name: 'Gábor', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 10, name: 'Zsolti', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 11, name: 'Tibike', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() },
//         { id: 12, name: 'Apach', email: 'semmi@semmi.hu', address: 'Teszt cím', birthdate: new Date() }];

//     constructor(private http: HttpClient) { }

//     getUsers(): Observable<User[]> {
//         return of(this.userList);
//     }

//     deleteUser(id: number): any {
//         this.userList.splice(
//             this.userList.findIndex(el => el.id === id),
//             1
//         );
//         localStorage.setItem('userList', JSON.stringify(this.userList));
//     }

//     getUser(pId: number): any {
//         return this.userList.find(el => el.id === pId);
//     }
//     /*getIndexId(pId: number): any {
//         return this.userList.findIndex(el => el.id === pId);
//     }*/

//     updateUser(pUser: User): any {
//         this.userList[this.userList.findIndex(el => el.id === pUser.id)] = pUser;
//         localStorage.setItem('userList', JSON.stringify(this.userList));
//     }

//     createUser(pNewUser: User) {
//         const newUser: User = pNewUser;

//         if (this.userList.length === 0) {
//             this.userIdBuffer = 0;
//         } else {
//             this.userIdBuffer = Number(Math.max.apply(Math, this.userList.map(el => el.id)) + 1);
//         }

//         // newUser.id = this.userIdBuffer;

//         this.userList.push(newUser);
//         localStorage.setItem('userList', JSON.stringify(this.userList));

//         return this.userIdBuffer;
//     }
// }
