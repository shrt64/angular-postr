import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'firebase/app';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    public user: User;
    constructor(
        public auth: AngularFireAuth,
        public firestore: AngularFirestore
    ) { }

    loginWithEmail(pFormValue: any): Promise<any> {
        console.log(pFormValue);
        let returnError: Promise<any>;
        returnError = this.auth.signInWithEmailAndPassword(pFormValue.username, pFormValue.password).then(pAuth => {
            localStorage.setItem('userEmail', JSON.stringify(pAuth.user.email));
            console.log('logged in as ' + pAuth.user.email);
            return '';
        }).catch(error => {
            console.log(error);
            return error;
        });
        return returnError;
    }

    registerWithEmail(pFormValue: any): Promise<any> {
        console.log(pFormValue);
        let returnError: Promise<any>;
        returnError = this.auth.createUserWithEmailAndPassword(pFormValue.username, pFormValue.password).then(pAuth => {
            localStorage.setItem('userEmail', JSON.stringify(pAuth.user.email));
            console.log('registered as ' + pAuth.user.email);
            console.log(pAuth.user);
            this.firestore.collection('users').doc(pAuth.user.uid).set({
                uid: pAuth.user.uid,
                email: pAuth.user.email,
                emailVerified: pAuth.user.emailVerified
            })
                .then(() => {
                    console.log('Document successfully written!');
                })
                .catch(error => {
                    console.error('Error writing document: ', error);
                });
            return '';
        }).catch(error => {
            console.log(error);
            return error;
        });
        return returnError;
    }

    logout() {
        this.auth.signOut().then(res => {
            console.log('Logged out');
        }).catch(error => {
            console.log(error);
        });
    }

    userState(): Observable<User> {
        return this.auth.user;
    }
}
