import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MenuItem } from '@model/menu-item';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    public menuIsVisible = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient) { }

    getMenuList(): Observable<MenuItem[]> {
        return of([
            { id: 1, name: 'Posztok', icon: 'users', link: 'Posts' }
        ]);

    }

    toggleMenuVisibility() {
        this.menuIsVisible.next(!this.menuIsVisible.getValue());
    }

    getMenuVisibility() {
        return this.menuIsVisible.asObservable();
    }
}
