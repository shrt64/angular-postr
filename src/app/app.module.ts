import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { MessageModule, MessagesModule } from 'primeng/primeng';
import { SidebarModule } from 'primeng/sidebar';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { MenuItemComponent } from './layout/menu/menu-item/menu-item.component';
import { MenuComponent } from './layout/menu/menu.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { PostListComponent } from './pages/post-list/post-list.component';
import { PostComponent } from './pages/post-list/post/post.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { ProfileComponent } from './pages/auth/profile/profile.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        MenuComponent,
        MenuItemComponent,
        PostListComponent,
        LoginComponent,
        PostComponent,
        RegisterComponent,
        ProfileComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FontAwesomeModule,
        TableModule,
        ReactiveFormsModule,
        FormsModule,
        ButtonModule,
        ToastModule,
        BrowserAnimationsModule,
        InputTextModule,
        MessagesModule,
        MessageModule,
        CalendarModule,
        SidebarModule,
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAnalyticsModule,
        AngularFirestoreModule,
        AngularFireAuthModule
    ],
    providers: [AngularFireAuth],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor() {
        // FontAwesome
        library.add(fas, far);
    }

}
